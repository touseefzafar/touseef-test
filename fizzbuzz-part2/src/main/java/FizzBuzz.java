/**
 * Created by IntelliJ IDEA.
 * User: ZafarT
 * Date: 18/05/17
 * Time: 12:39
 * To change this template use File | Settings | File Templates.
 */
public class FizzBuzz {
    public boolean isFizz(int a) {
        if(a % 3 == 0) return true;
        return false;
    }

    public boolean isBuzz(int a) {
        if(a % 5 == 0) return true;
        return false;
    }

    public boolean isFizzBuzz(int a) {
        if(a % 3 == 0 && a %5 == 0) return true;
        return false;
    }

    public String processRange(int start, int end) {
        String output = "";
        for(int i = start ; i <= end ; i++){
            if(isLucky(i)){
                output += "lucky ";
            } else if(isFizzBuzz(i)){
                output += "fizzbuzz ";
            } else if(isFizz(i)){
                output += "fizz ";
            } else if(isBuzz(i)){
                output += "buzz ";
            } else {
                output += i + " ";
            }
        }

        return output.trim();
    }

    public boolean isLucky(int a) {
        String input = a + "";
        if(input.indexOf("3") > -1) return true;
        return false;
    }
}
