import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by IntelliJ IDEA.
 * User: ZafarT
 * Date: 18/05/17
 * Time: 12:37
 * To change this template use File | Settings | File Templates.
 */
public class TestFizzBuzz {
    FizzBuzz fizzBuzz;

    @Before
    public void setup(){
        fizzBuzz = new FizzBuzz();
        fizzBuzz.setStart(1);
        fizzBuzz.setEnd(20);
    }

    @Test
    public void testInputRange(){
        Assert.assertEquals(true,fizzBuzz.isInputValid());
    }

    @Test
    public void testIfFizz(){
        int a = 3;
        Assert.assertEquals(true,fizzBuzz.isFizz(a));
    }

    @Test
    public void testIfBuzz(){
        int a = 5;
        Assert.assertEquals(true,fizzBuzz.isBuzz(a));
    }

    @Test
    public void testIfFizzBuzz(){
        int a = 15;
        Assert.assertEquals(true,fizzBuzz.isFizzBuzz(a));
    }

    @Test
    public void testFizzBuzzProcess(){
       String expectedOutput = "1 2 lucky 4 buzz fizz 7 8 fizz buzz 11 fizz lucky 14 fizzbuzz 16 17 fizz 19 buzz";
       String actuallOutput = fizzBuzz.processRange();
       Assert.assertEquals(true,expectedOutput.equals(actuallOutput));
    }

    @Test
    public void testIfLucky(){
        int a = 13;
        Assert.assertEquals(true,fizzBuzz.isLucky(a));
    }

    @Test
    public void testFizzCount(){
        String output = fizzBuzz.processRange();
        Assert.assertEquals(4, fizzBuzz.countElements(output,"fizz"));

    }

    @Test
    public void testBuzzCount(){
        String output = fizzBuzz.processRange();
        Assert.assertEquals(3, fizzBuzz.countElements(output,"buzz"));

    }

    @Test
    public void testFizzBuzzCount(){
        String output = fizzBuzz.processRange();
        Assert.assertEquals(1, fizzBuzz.countElements(output,"fizzbuzz"));
    }

    @Test
    public void testLuckyCount(){
        String output = fizzBuzz.processRange();
        Assert.assertEquals(2, fizzBuzz.countElements(output,"lucky"));
    }

    @Test
    public void testIntegerCount(){
        String output = fizzBuzz.processRange();
        Assert.assertEquals(10, fizzBuzz.countElements(output,"integer"));
    }



}
