import com.sun.deploy.util.StringUtils;
import com.sun.javafx.collections.MappingChange;

import java.lang.reflect.Array;
import java.util.HashMap;

/**
 * Created by IntelliJ IDEA.
 * User: ZafarT
 * Date: 18/05/17
 * Time: 12:39
 * To change this template use File | Settings | File Templates.
 */
public class FizzBuzz {
    private int start;
    private int end;


    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public boolean isFizz(int a) {
        if (a % 3 == 0) return true;
        return false;
    }

    public boolean isBuzz(int a) {
        if (a % 5 == 0) return true;
        return false;
    }

    public boolean isFizzBuzz(int a) {
        if (a % 3 == 0 && a % 5 == 0) return true;
        return false;
    }

    public String processRange() {
        String output = "";
        for (int i = start; i <= end; i++) {
            if (isLucky(i)) {
                output += "lucky ";
            } else if (isFizzBuzz(i)) {
                output += "fizzbuzz ";
            } else if (isFizz(i)) {
                output += "fizz ";
            } else if (isBuzz(i)) {
                output += "buzz ";
            } else {
                output += i + " ";
            }
        }

        return output.trim();
    }

    public boolean isLucky(int a) {
        String input = a + "";
        if (input.indexOf("3") > -1) return true;
        return false;
    }

    public int countElements(String output, String type) {
        int count = 0;
        Object[] explodedOutput = output.split(" ");
        for (Object str : explodedOutput) {
            if (str instanceof String && str.equals(type)) {
                count++;
            } else if (isNumeric(str) && type.equals("integer")) {
                count++;

            }
        }

        return count;
    }

    private boolean isNumeric(Object obj) {
        try {
            Double d = Double.parseDouble(obj.toString());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /*
    * Assumption:
    * */
    public boolean isInputValid() {
        return this.start < this.end;
    }
}
