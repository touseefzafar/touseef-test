/**
 * Created by IntelliJ IDEA.
 * User: ZafarT
 * Date: 18/05/17
 * Time: 12:53
 * To change this template use File | Settings | File Templates.
 */
public class Application {

    public static void main(String... args){
        FizzBuzz fizzBuzz = new FizzBuzz();
        String output = fizzBuzz.processRange();
        System.out.println(output);
    }
}
