import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by IntelliJ IDEA.
 * User: ZafarT
 * Date: 18/05/17
 * Time: 12:37
 * To change this template use File | Settings | File Templates.
 */
public class TestFizzBuzz {
    private int start;
    private int end;
    FizzBuzz fizzBuzz;

    @Before
    public void setup(){
        start = 1;
        end = 20;
        fizzBuzz = new FizzBuzz();
    }

    @Test
    public void testIfFizz(){
        int a = 3;
        Assert.assertEquals(true,fizzBuzz.isFizz(a));
    }

    @Test
    public void testIfBuzz(){
        int a = 5;
        Assert.assertEquals(true,fizzBuzz.isBuzz(a));
    }

    @Test
    public void testIfFizzBuzz(){
        int a = 15;
        Assert.assertEquals(true,fizzBuzz.isFizzBuzz(a));
    }

    @Test
    public void testFizzBuzzProcess(){
       String expectedOutput = "1 2 fizz 4 buzz fizz 7 8 fizz buzz 11 fizz 13 14 fizzbuzz 16 17 fizz 19 buzz";
       String actuallOutput = fizzBuzz.processRange(start,end);
       Assert.assertEquals(true,expectedOutput.equals(actuallOutput));
    }


}
